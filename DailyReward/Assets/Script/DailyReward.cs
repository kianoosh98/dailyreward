using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DailyReward : MonoBehaviour
{
    public int coin;
    public int day;
    public int lastDay;
    public int nextRewardDelay = 10;
    public int claimReward = 0;

    public Sprite defaultRewardBg;
    public Sprite deactiveRewardBg;
    public Sprite activetRewardBg;
    public Sprite grayBtn;
    public Sprite goldBtn;

    public Text coinTxt;
    public Text[] valueOfRewardText;
    public Text[] dayXText;

    public Image[] avatar;

    public Button[] claimBtn;

    public GameObject dailyRewardGO;
    public GameObject coinPrefab;
    public GameObject coinParent;

    public DateTime firstTime;

    private void Start()
    {
        if(string.IsNullOrEmpty(PlayerPrefs.GetString("First_Time_DateTime")))
        {
            PlayerPrefs.SetString("First_Time_DateTime", DateTime.Now.ToString());
            PlayerPrefs.SetInt("Count_Of_Day", day);
            PlayerPrefs.SetInt("Count_Of_LastDay", lastDay);
            PlayerPrefs.SetInt("Count_Of_Coins", coin);
            PlayerPrefs.SetInt("Claim_Reward", claimReward);
        }
        claimReward = PlayerPrefs.GetInt("Claim_Reward");
        coin = PlayerPrefs.GetInt("Count_Of_Coins");
        EarnCoin(0);
        firstTime = DateTime.Parse(PlayerPrefs.GetString("First_Time_DateTime"));
        day = PlayerPrefs.GetInt("Count_Of_Day");
        lastDay = PlayerPrefs.GetInt("Count_Of_LastDay");

        UpdateDailyRewardPanel();
        UpdateReward();
    }

    public void UpdateDailyRewardPanel()
    {
        if(day > 5)
        {
            int index = day % 6;
            int c = 5 - index;
            float d = day + c;
            for(int i = 5; i >= 0; i--)
            {
                dayXText[i].text = "day " + (d + 1);
                float m = d / 5;
                valueOfRewardText[i].text = (2500 * m).ToString();
                d--;
            }
        }
    }
    public void DayCounter()
    {
        DateTime currentTime = DateTime.Now;
        double elapsed = (currentTime - firstTime).TotalSeconds;
        lastDay = day;
        PlayerPrefs.SetInt("Count_Of_LastDay", lastDay);
        day = (int)elapsed / nextRewardDelay;
        PlayerPrefs.SetInt("Count_Of_Day", day);
        if(lastDay < day)
        {
            claimReward = 0;
            PlayerPrefs.SetInt("Claim_Reward", claimReward);
            UpdateReward();
        }
    }
    public void UpdateReward()
    {
        if(claimReward == 0)
        {
            if(day > 5)
            {
                UpdateDailyRewardPanel();
                DeActiveReward(lastDay % 6);
                ActiveReward(day % 6);

            }
            else if(day == 0)
            {
                ActiveReward(day);
            }
            else
            {
                DeActiveReward(lastDay);
                ActiveReward(day);
            }
        }
    }
    public void ClaimBtn(int i)
    {
        AnimationCoin(i, int.Parse(valueOfRewardText[i].text));
        DeActiveReward(i);
        claimReward = 1;
        PlayerPrefs.SetInt("Claim_Reward", claimReward);
    }

    public void ActiveReward(int i)
    {
        avatar[i].sprite = activetRewardBg;
        claimBtn[i].image.sprite = goldBtn;
        claimBtn[i].interactable = true;
    }
    public void DeActiveReward(int i)
    {
        avatar[i].sprite = deactiveRewardBg;
        claimBtn[i].image.sprite = grayBtn;
        claimBtn[i].interactable = false;
    }
    public void Update()
    {
        DayCounter();
    }
    public void OnDailyRewardBtn()
    {
        dailyRewardGO.SetActive(true);
    }
    public void OnCloseDailyReward()
    {
        dailyRewardGO.SetActive(false);
    }
    public void EarnCoin(int i)
    {
        coin += i;
        coinTxt.text = coin.ToString();
        PlayerPrefs.SetInt("Count_Of_Coins", coin);
    }

    public void AnimationCoin(int i, int number)
    {
        GameObject coinGO = Instantiate(coinPrefab, avatar[i].transform);
        StartCoroutine(CoinMovement(coinGO, number));
    }
    public IEnumerator CoinMovement(GameObject coinGO, int number)
    {
        float timeToStart = Time.time;
        bool position = false;
        yield return new WaitForSeconds(0.4f);
        coinGO.transform.parent = coinParent.transform;
        while(!position)
        {
            if(!position)
            {
                coinGO.transform.localPosition = Vector3.MoveTowards(coinGO.transform.localPosition, new Vector3(0, 0, 0), (Time.time - timeToStart) * 10f);
                if(Vector3.Distance(coinGO.transform.localPosition, new Vector3(0, 0, 0)) < 0.01f)
                {
                    position = true;
                    coinGO.transform.localPosition = new Vector3(0, 0, 0);
                    Destroy(coinGO.gameObject);
                    coinParent.GetComponent<Animator>().SetTrigger("claim");
                    EarnCoin(number);
                }
            }
            yield return null;
        }
    }
}
